# TroubleTrack

TroubleTrack is a bug tracking and error reporting platform designed to assist developers in tracking, managing, and
resolving frontend errors efficiently.

## Table of Contents

- [Project Overview](#project-overview)
- [Prerequisites](#prerequisites)
- [Installation](#installation)
- [Running the Application](#running-the-application)
- [API Endpoints](#api-endpoints)

## Project Overview

The TroubleTrack platform is a bug tracking and error reporting system designed to assist developers in tracking,
managing, and resolving frontend errors efficiently.

## Prerequisites

- Docker installed ([installation guide](https://docs.docker.com/engine/install/))
- Docker Compose installed ([installation guide](https://docs.docker.com/compose/install/))

## Installation

1. **Clone the repository:**
    ```bash
    git clone https://github.com/your-username/troubletrack.git
    ```

2. **Navigate to the project directory:**
    ```bash
    cd troubletrack
    ```

3. **Install dependencies:**
    ```bash
    npm install
    ```

## Running the Application

1. **Copy Env File:**
    ```bash
    cp env.example .env
    ```

2. **Build the docker images:**
    ```bash
    docker-compose build
    ```

3. **Start the application:**
    ```bash
    docker-compose up
    ```

   This will start the application and all its dependencies in detached mode. You can access the application in your
   browser at [http://localhost:3000](http://localhost:3000) (assuming the default port is not changed).

## API Endpoints

The TroubleTrack platform provides several API endpoints for managing errors:

- **POST /api/projects/{projectId}/errors**: Endpoint for reporting errors occurring in a specific project. Requires
  authentication and accepts a JSON payload with error details (type, timestamp, etc.). Returns a success message or
  error response.

- **GET /api/projects/{projectId}/errors**: Endpoint for retrieving all reported errors for a specific project. Requires
  authentication. Returns a list of errors with details such as type, timestamp, status, etc.

- **PUT /api/projects/{projectId}/errors/{errorId}**: Endpoint for updating the status of a reported error. Requires
  authentication and accepts a JSON payload with updated error status. Returns a success message or error response.

- **DELETE /api/projects/{projectId}/errors/{errorId}**: Endpoint for deleting a reported error. Requires
  authentication. Returns a success message or error response.
