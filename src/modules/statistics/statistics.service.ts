import { Injectable } from '@nestjs/common';
import { BugsService } from '../bugs/bugs.service';
import { Project } from '../projects/entities/project.entity';
import { ProjectsService } from '../projects/projects.service';

export interface BugsTotalCount {
  totalCount: number;
}

export interface ProjectAverageResolutionTime {
  projectId: number;
  averageResolutionTime: string;
}

export interface AverageResolutionTime {
  averageResolutionTime: string;
}

export interface BugCountBySeverity {
  severity: string;
  count: number;
}

export interface ProjectBugsTypeDistribution {
  projectId: number;
  errorDistribution: { errorType: any; count: any }[];
}

@Injectable()
export class StatisticsService {
  constructor(
    private readonly bugsService: BugsService,
    private readonly projectService: ProjectsService,
  ) {}

  async countTotalBugs(): Promise<BugsTotalCount> {
    const totalCount = await this.bugsService.count();

    return {
      totalCount,
    };
  }

  async getProjectsAverageResolutionTimes(): Promise<
    ProjectAverageResolutionTime[]
  > {
    const projects = await this.projectService.findAll();

    return await Promise.all(
      projects.map(async (project: Project) => {
        const projectId = project.id;
        const averageResolutionTime =
          await this.bugsService.calculateProjectAverageResolutionTime(
            projectId,
          );
        return { projectId, averageResolutionTime };
      }),
    );
  }

  async getAverageResolutionTime(): Promise<AverageResolutionTime> {
    const averageResolutionTime =
      await this.bugsService.calculateAverageResolutionTime();

    return {
      averageResolutionTime,
    };
  }

  async groupErrorsBySeverity(): Promise<BugCountBySeverity[]> {
    return await this.bugsService.getBugSeverityDistribution();
  }

  async getProjectBugsTypeDistribution(
    projectId: number,
  ): Promise<ProjectBugsTypeDistribution> {
    const errorDistribution =
      await this.bugsService.getProjectBugsTypeDistribution(projectId);

    return {
      projectId,
      errorDistribution,
    };
  }
}
