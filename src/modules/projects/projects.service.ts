import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Project } from './entities/project.entity';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { BugsService } from '../bugs/bugs.service';
import { Bug } from '../bugs/entities';
import { CreateProjectBugDto } from '../bugs/dto';
import { BUG_NOT_FOUND, PROJECT_NOT_FOUND } from '../../errors';

@Injectable()
export class ProjectsService {
  constructor(
    @InjectRepository(Project)
    private readonly projectRepository: Repository<Project>,
    private readonly bugsService: BugsService,
  ) {}

  async createProject(createProjectDto: CreateProjectDto): Promise<Project> {
    const newProject = this.projectRepository.create(createProjectDto);
    return await this.projectRepository.save(newProject);
  }

  async findAll(): Promise<Project[]> {
    return await this.projectRepository.find();
  }

  async findById(id: number): Promise<Project | null> {
    return await this.projectRepository.findOne({
      where: {
        id,
      },
    });
  }

  async updateProject(
    id: number,
    updateProjectDto: UpdateProjectDto,
  ): Promise<Project | undefined> {
    const project = await this.findById(id);
    if (!project) return undefined;

    const updatedProject = Object.assign(project, updateProjectDto);
    return await this.projectRepository.save(updatedProject);
  }

  async deleteProject(id: number): Promise<boolean> {
    const project = await this.findById(id);
    if (!project) {
      throw new NotFoundException(PROJECT_NOT_FOUND);
    }

    await this.projectRepository.softDelete(project.id);
    return true;
  }

  async reportProjectBug(
    projectId: number,
    bugData: CreateProjectBugDto,
  ): Promise<Bug> {
    const foundProject = await this.findById(projectId);
    if (!foundProject) {
      throw new NotFoundException(PROJECT_NOT_FOUND);
    }

    return await this.bugsService.createBug({
      project: foundProject,
      ...bugData,
    });
  }

  async getProjectBugs(projectId: number): Promise<any[]> {
    const foundProject = await this.findById(projectId);
    if (!foundProject) {
      throw new NotFoundException(PROJECT_NOT_FOUND);
    }

    return await this.bugsService.findAll({
      project: foundProject,
    });
  }

  async updateProjectBug(bugId: number, updateBugData: any): Promise<Bug> {
    const updatedBug = await this.bugsService.updateBug(bugId, updateBugData);

    if (!updatedBug) {
      throw new NotFoundException(BUG_NOT_FOUND);
    }

    return updatedBug;
  }

  async deleteProjectBug(errorId: number): Promise<boolean> {
    return await this.bugsService.deleteBug(errorId);
  }
}
