import { PartialType } from '@nestjs/mapped-types';

import { CreateBugDto } from './create-bug.dto';
import { OmitType } from '@nestjs/swagger';

export class UpdateBugDto extends PartialType(
  OmitType(CreateBugDto, ['project']),
) {}
