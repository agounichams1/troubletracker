import { MigrationInterface, QueryRunner } from "typeorm";

export class AddBugResolvedAt1715168553138 implements MigrationInterface {
    name = 'AddBugResolvedAt1715168553138'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "bug" ADD "resolvedAt" TIMESTAMP WITH TIME ZONE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "bug" DROP COLUMN "resolvedAt"`);
    }

}
