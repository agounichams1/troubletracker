export function formatTime(averageTimeInMs: number): string {
  const years = Math.floor(averageTimeInMs / (1000 * 60 * 60 * 24 * 365));
  const months = Math.floor(
    (averageTimeInMs % (1000 * 60 * 60 * 24 * 365)) /
      (1000 * 60 * 60 * 24 * 30),
  );
  const days = Math.floor(
    (averageTimeInMs % (1000 * 60 * 60 * 24 * 30)) / (1000 * 60 * 60 * 24),
  );
  const hours = Math.floor(
    (averageTimeInMs % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60),
  );

  let formattedTime: string;
  if (years > 0) {
    formattedTime = `${years} year${years > 1 ? 's' : ''}`;
  } else if (months > 0) {
    formattedTime = `${months} month${months > 1 ? 's' : ''}`;
  } else if (days > 0) {
    formattedTime = `${days} day${days > 1 ? 's' : ''}`;
  } else {
    formattedTime = `${hours} hour${hours > 1 ? 's' : ''}`;
  }

  return formattedTime;
}
