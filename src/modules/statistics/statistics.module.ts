import { Module } from '@nestjs/common';

import { StatisticsService } from './statistics.service';
import { StatisticsController } from './statistics.controller';
import { BugsModule } from '../bugs/bugs.module';
import { ProjectsModule } from '../projects/projects.module';

@Module({
  imports: [BugsModule, ProjectsModule],
  controllers: [StatisticsController],
  providers: [StatisticsService],
})
export class StatisticsModule {}
