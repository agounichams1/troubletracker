import {
  Column,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
} from 'typeorm';

import { BaseEntity } from '../../../common/base/entities';
import { SoftDeletable } from '../../../common/types';
import { Project } from '../../projects/entities/project.entity';
import { BugSeverityEnum } from '../enums';

@Entity()
export class Bug extends BaseEntity implements SoftDeletable {
  @ManyToOne(() => Project, { cascade: true, onDelete: 'CASCADE' })
  @JoinColumn({ referencedColumnName: 'id', name: 'project_id' })
  project: Project;

  @Column({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  timestamp: Date;

  @Column({ type: 'timestamptz', nullable: true })
  resolvedAt: Date;

  @Column({ nullable: true })
  message?: string;

  @Column({ name: 'exception_type' })
  exceptionType: string;

  @Column()
  stacktrace: string;

  @Column({ nullable: true })
  release?: string;

  @Column({ nullable: true })
  environment?: string;

  @Column({ nullable: true })
  level?: string;

  @Column({
    type: 'enum',
    enum: BugSeverityEnum,
    default: BugSeverityEnum.Minor,
  })
  severity: BugSeverityEnum;

  @DeleteDateColumn()
  deletedAt: Date;
}
