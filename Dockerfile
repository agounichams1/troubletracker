FROM node:18-alpine AS build

WORKDIR /app
COPY package*.json ./
RUN yarn install
COPY . ./
# build js & remove devDependencies from node_modules
RUN yarn build


FROM node:18-alpine

ENV PORT=3000
ENV NODE_ENV=production
WORKDIR /app

COPY --from=build /app/dist /app/dist
COPY --from=build /app/node_modules /app/node_modules

RUN rm -rf /app/dist/common/database/migrations/*.d.ts /app/dist/common/database/migrations/*.map
COPY --from=build /app/package.json /app/package.json

EXPOSE 3000
ENTRYPOINT [ "node" ]
CMD [ "dist/main.js" ]