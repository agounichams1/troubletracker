import { IsNumber } from 'class-validator';

import { CreateUserDto } from './create-user.dto';
import { OmitType, PartialType } from '@nestjs/swagger';

export class UpdateUserDto extends PartialType(
  OmitType(CreateUserDto, ['email', 'isActive', 'password']),
) {
  @IsNumber()
  id: number;
}
