import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEnum,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

import { BaseEntity } from '../../../common/base/entities';
import { Project } from '../../projects/entities/project.entity';
import { BugSeverityEnum } from '../enums';

export class CreateBugDto extends BaseEntity {
  @ApiProperty({ description: 'Project ID' })
  @IsInt()
  @IsNotEmpty()
  project: Project;

  @ApiProperty({ description: 'Timestamp of the bug occurrence' })
  @IsNotEmpty()
  timestamp: Date;

  @ApiPropertyOptional({ description: 'Bug message' })
  @IsOptional()
  @IsString()
  message?: string;

  @ApiProperty({ description: 'Exception type' })
  @IsNotEmpty()
  @IsString()
  exceptionType: string;

  @ApiProperty({ description: 'Bug stacktrace' })
  @IsNotEmpty()
  @IsString()
  stacktrace: string;

  @ApiPropertyOptional({ description: 'Release version' })
  @IsOptional()
  @IsString()
  release?: string;

  @ApiPropertyOptional({ description: 'Environment where the bug occurred' })
  @IsOptional()
  @IsString()
  environment?: string;

  @ApiPropertyOptional({ description: 'Bug level (e.g., error, warning)' })
  @IsOptional()
  @IsString()
  level?: string;

  @ApiPropertyOptional({
    description: 'Bug severity (e.g., critical, major)',
    enum: BugSeverityEnum,
  })
  @IsEnum(BugSeverityEnum)
  severity: BugSeverityEnum;
}
