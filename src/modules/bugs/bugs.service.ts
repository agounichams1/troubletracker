import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Bug } from './entities';
import { CreateBugDto, FilterBugsDto, UpdateBugDto } from './dto';
import { formatTime } from '../../utils/format-time';
import { BugCountBySeverity } from '../statistics/statistics.service';
import { BUG_NOT_FOUND } from '../../errors';

@Injectable()
export class BugsService {
  constructor(
    @InjectRepository(Bug)
    private readonly bugRepository: Repository<Bug>,
  ) {}

  async createBug(createBugDto: CreateBugDto): Promise<Bug> {
    const newBug = this.bugRepository.create(createBugDto);
    return await this.bugRepository.save(newBug);
  }

  async findAll(filterBugsDto?: FilterBugsDto): Promise<Bug[]> {
    return await this.bugRepository.find({
      where: filterBugsDto,
    });
  }

  async findById(id: number): Promise<Bug | null> {
    return await this.bugRepository.findOne({
      where: {
        id,
      },
    });
  }

  async updateBug(
    id: number,
    updateBugDto: UpdateBugDto,
  ): Promise<Bug | undefined> {
    const bug = await this.findById(id);
    if (!bug) return undefined;

    const updatedBug = Object.assign(bug, updateBugDto);
    return await this.bugRepository.save(updatedBug);
  }

  async deleteBug(id: number): Promise<boolean> {
    const bug = await this.findById(id);
    if (!bug) {
      throw new NotFoundException(BUG_NOT_FOUND);
    }

    await this.bugRepository.softDelete(bug.id);
    return true;
  }

  async count(): Promise<number> {
    return await this.bugRepository.count();
  }

  async calculateProjectAverageResolutionTime(
    projectId: number,
  ): Promise<string> {
    const averageTimeInMs =
      await this.getProjectsAverageResolutionTimeInMs(projectId);
    return formatTime(averageTimeInMs);
  }

  async calculateAverageResolutionTime(): Promise<string> {
    const averageTimeInMs = await this.getAverageResolutionTimeInMs();
    return formatTime(averageTimeInMs);
  }

  public async getAverageResolutionTimeInMs(): Promise<number> {
    const queryBuilder = this.bugRepository.createQueryBuilder('bug');
    return await this.calculateAverageResolutionTimeInMs(queryBuilder);
  }

  public async getProjectsAverageResolutionTimeInMs(
    projectId: number,
  ): Promise<number> {
    const queryBuilder = this.bugRepository.createQueryBuilder('bug');
    queryBuilder.where('bug.project_id = :projectId', { projectId });
    return await this.calculateAverageResolutionTimeInMs(queryBuilder);
  }

  async getBugSeverityDistribution(): Promise<BugCountBySeverity[]> {
    const queryBuilder = this.bugRepository.createQueryBuilder('bug');

    const result = await queryBuilder
      .select('bug.severity', 'severity')
      .addSelect('COUNT(bug.id)', 'count')
      .groupBy('bug.severity')
      .getRawMany();

    return result.map((row) => ({
      severity: row.severity,
      count: row.count,
    }));
  }

  async getProjectBugsTypeDistribution(projectId: number) {
    const queryBuilder = this.bugRepository.createQueryBuilder('bug');

    const result = await queryBuilder
      .where('bug.project_id = :projectId', { projectId })
      .select('bug.exceptionType', 'exceptionType')
      .addSelect('COUNT(bug.id)', 'count')
      .groupBy('bug.exceptionType')
      .getRawMany();

    return result.map((row) => ({
      errorType: row.exceptionType,
      count: row.count,
    }));
  }

  private async calculateAverageResolutionTimeInMs(
    queryBuilder: any,
  ): Promise<number> {
    queryBuilder.select(
      'AVG(EXTRACT(EPOCH FROM (bug."resolvedAt"::timestamp - bug."timestamp"::timestamp)) * 1000)',
      'averageResolutionTime',
    );

    const result = await queryBuilder.getRawOne();

    return result?.averageResolutionTime || 0;
  }
}
