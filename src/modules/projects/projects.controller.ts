import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  ParseIntPipe,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import { ProjectsService } from './projects.service';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { Project } from './entities/project.entity';
import { JwtAuthGuard } from '../auth/guards';
import { BUG_NOT_FOUND, PROJECT_NOT_FOUND } from '../../errors';
import { Bug } from '../bugs/entities';
import { CreateProjectBugDto, UpdateBugDto } from '../bugs/dto';

@ApiTags('Projects')
@Controller('projects')
export class ProjectsController {
  constructor(private readonly projectsService: ProjectsService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Create a new project' })
  @ApiResponse({
    status: 201,
    description: 'The created project',
    type: Project,
  })
  async createProject(
    @Body() createProjectDto: CreateProjectDto,
  ): Promise<Project> {
    return await this.projectsService.createProject(createProjectDto);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get all projects' })
  @ApiResponse({
    status: 200,
    description: 'An array of projects',
    type: Project,
    isArray: true,
  })
  async findAll(): Promise<Project[]> {
    return await this.projectsService.findAll();
  }

  @Get(':projectId')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get a project by ID' })
  @ApiResponse({
    status: 200,
    description: 'The project with the specified ID',
    type: Project,
  })
  @ApiResponse({ status: 404, description: 'Project not found' })
  async findById(
    @Param('projectId', ParseIntPipe) projectId: number,
  ): Promise<Project | undefined> {
    const project = await this.projectsService.findById(projectId);

    if (!project) {
      throw new NotFoundException(PROJECT_NOT_FOUND);
    }

    return project;
  }

  @Put(':projectId')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Update a project by ID' })
  @ApiResponse({
    status: 200,
    description: 'The updated project',
    type: Project,
  })
  @ApiResponse({ status: 404, description: 'Project not found' })
  async updateProject(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Body() updateProjectDto: UpdateProjectDto,
  ): Promise<Project | undefined> {
    return await this.projectsService.updateProject(
      projectId,
      updateProjectDto,
    );
  }

  @Delete(':projectId')
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiOperation({ summary: 'Delete a project by ID' })
  @ApiResponse({ status: 200, description: 'Project deleted' })
  @ApiResponse({ status: 404, description: 'Project not found' })
  async deleteProject(
    @Param('projectId', ParseIntPipe) projectId: number,
  ): Promise<boolean> {
    return await this.projectsService.deleteProject(projectId);
  }

  @Post(':projectId/errors')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Report a bug in a project' })
  @ApiResponse({ status: 201, description: 'Created bug', type: Bug })
  @ApiResponse({ status: 404, description: PROJECT_NOT_FOUND })
  async reportProjectBug(
    @Param('projectId', ParseIntPipe) projectId: number,
    @Body() bugData: CreateProjectBugDto,
  ): Promise<Bug> {
    return await this.projectsService.reportProjectBug(projectId, bugData);
  }

  @Get(':projectId/errors')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get all bugs for a project' })
  @ApiResponse({
    status: 200,
    description: 'List of bugs',
    type: Bug,
    isArray: true,
  })
  @ApiResponse({ status: 404, description: PROJECT_NOT_FOUND })
  async getProjectBugs(
    @Param('projectId', ParseIntPipe) projectId: number,
  ): Promise<Bug[]> {
    return await this.projectsService.getProjectBugs(projectId);
  }

  @Put(':projectId/errors/:errorId')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Update a bug in a project' })
  @ApiResponse({ status: 200, description: 'Updated bug', type: Bug })
  @ApiResponse({ status: 404, description: BUG_NOT_FOUND })
  async updateProjectBug(
    @Param('errorId', ParseIntPipe) bugId: number,
    @Body() updateBugData: UpdateBugDto,
  ): Promise<Bug> {
    return await this.projectsService.updateProjectBug(bugId, updateBugData);
  }

  @Delete(':projectId/errors/:errorId')
  @UseGuards(JwtAuthGuard)
  @HttpCode(HttpStatus.NO_CONTENT)
  @ApiOperation({ summary: 'Delete a bug in a project' })
  @ApiResponse({ status: 200, description: 'Bug deleted' })
  @ApiResponse({ status: 404, description: BUG_NOT_FOUND })
  async deleteProjectError(
    @Param('errorId', ParseIntPipe) bugId: number,
  ): Promise<boolean> {
    return await this.projectsService.deleteProjectBug(bugId);
  }
}
