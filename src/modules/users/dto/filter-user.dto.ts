import { PartialType, PickType } from '@nestjs/swagger';

import { CreateUserDto } from './create-user.dto';

export class FilterUsersDto extends PartialType(
  PickType(CreateUserDto, ['isActive']),
) {}
