import { Controller, Get, Param, ParseIntPipe, UseGuards } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';

import {
  AverageResolutionTime,
  BugCountBySeverity,
  BugsTotalCount,
  ProjectBugsTypeDistribution,
  StatisticsService,
} from './statistics.service';
import { JwtAuthGuard } from '../auth/guards';

@Controller('statistics')
@ApiTags('statistics')
export class StatisticsController {
  constructor(private readonly statisticsService: StatisticsService) {}

  @UseGuards(JwtAuthGuard)
  @Get(':projectId/errorsTypeDistribution')
  @ApiOperation({ summary: 'Get bug type distribution for a project' })
  @ApiResponse({ status: 200 })
  async getProjectBugsTypeDistribution(
    @Param('projectId', ParseIntPipe) projectId: number,
  ): Promise<ProjectBugsTypeDistribution> {
    return await this.statisticsService.getProjectBugsTypeDistribution(
      projectId,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get('projectsAverageResolutionTimes')
  @ApiOperation({ summary: 'Get average resolution time for all projects' })
  @ApiResponse({ status: 200 })
  async getProjectsAverageResolutionTimes(): Promise<AverageResolutionTime[]> {
    return await this.statisticsService.getProjectsAverageResolutionTimes();
  }

  @UseGuards(JwtAuthGuard)
  @Get('averageResolutionTimes')
  @ApiOperation({ summary: 'Get average resolution time overall' })
  @ApiResponse({ status: 200 })
  async getAverageResolutionTime(): Promise<AverageResolutionTime> {
    return await this.statisticsService.getAverageResolutionTime();
  }

  @UseGuards(JwtAuthGuard)
  @Get('errorsSeverityDistribution')
  @ApiOperation({ summary: 'Get bug count distribution by severity' })
  @ApiResponse({ status: 200, isArray: true })
  async groupBugsBySeverity(): Promise<BugCountBySeverity[]> {
    return await this.statisticsService.groupErrorsBySeverity();
  }

  @UseGuards(JwtAuthGuard)
  @Get('errorsCount')
  @ApiOperation({ summary: 'Get total bug count' })
  @ApiResponse({ status: 200 })
  async BugsCount(): Promise<BugsTotalCount> {
    return await this.statisticsService.countTotalBugs();
  }
}
