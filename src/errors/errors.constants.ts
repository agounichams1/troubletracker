export const USER_NOT_FOUND = '404001: USER NOT FOUND';
export const BUG_NOT_FOUND = '404002: BUG Not Found';
export const PROJECT_NOT_FOUND = '404003: PROJECT Not Found';

export const INVALID_CREDENTIALS = '401001: Invalid credentials';
