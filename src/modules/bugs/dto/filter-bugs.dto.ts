import { PartialType } from '@nestjs/swagger';

import { CreateBugDto } from './create-bug.dto';

export class FilterBugsDto extends PartialType(CreateBugDto) {}
