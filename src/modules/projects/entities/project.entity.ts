import { Column, DeleteDateColumn, Entity } from 'typeorm';

import { BaseEntity } from '../../../common/base/entities';
import { SoftDeletable } from '../../../common/types';

@Entity()
export class Project extends BaseEntity implements SoftDeletable {
  @Column()
  name: string;

  @DeleteDateColumn()
  deletedAt: Date;
}
