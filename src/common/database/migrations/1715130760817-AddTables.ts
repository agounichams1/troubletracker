import { MigrationInterface, QueryRunner } from "typeorm";

export class AddTables1715130760817 implements MigrationInterface {
    name = 'AddTables1715130760817'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "users" ("id" SERIAL NOT NULL, "first_name" character varying NOT NULL, "last_name" character varying NOT NULL, "email" character varying NOT NULL, "password" character varying NOT NULL, "is_active" boolean NOT NULL DEFAULT true, "deletedAt" TIMESTAMP, CONSTRAINT "UQ_97672ac88f789774dd47f7c8be3" UNIQUE ("email"), CONSTRAINT "PK_a3ffb1c0c8416b9fc6f907b7433" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_a3ffb1c0c8416b9fc6f907b743" ON "users" ("id") `);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_97672ac88f789774dd47f7c8be" ON "users" ("email") `);
        await queryRunner.query(`CREATE TABLE "project" ("id" SERIAL NOT NULL, "name" character varying NOT NULL, "deletedAt" TIMESTAMP, CONSTRAINT "PK_4d68b1358bb5b766d3e78f32f57" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_4d68b1358bb5b766d3e78f32f5" ON "project" ("id") `);
        await queryRunner.query(`CREATE TABLE "bug" ("id" SERIAL NOT NULL, "timestamp" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now(), "message" character varying, "exception_type" character varying NOT NULL, "stacktrace" character varying NOT NULL, "release" character varying, "environment" character varying, "level" character varying, "deletedAt" TIMESTAMP, "project_id" integer, CONSTRAINT "PK_9e7f67c6911b62a81ac3e336d4b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE UNIQUE INDEX "IDX_9e7f67c6911b62a81ac3e336d4" ON "bug" ("id") `);
        await queryRunner.query(`ALTER TABLE "bug" ADD CONSTRAINT "FK_e60f463a5b66f08c99d7cfacc96" FOREIGN KEY ("project_id") REFERENCES "project"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "bug" DROP CONSTRAINT "FK_e60f463a5b66f08c99d7cfacc96"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_9e7f67c6911b62a81ac3e336d4"`);
        await queryRunner.query(`DROP TABLE "bug"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_4d68b1358bb5b766d3e78f32f5"`);
        await queryRunner.query(`DROP TABLE "project"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_97672ac88f789774dd47f7c8be"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_a3ffb1c0c8416b9fc6f907b743"`);
        await queryRunner.query(`DROP TABLE "users"`);
    }

}
