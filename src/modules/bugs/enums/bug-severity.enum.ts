export enum BugSeverityEnum {
  Critical = 'Critical',
  Major = 'Major',
  Minor = 'Minor',
}
