import { OmitType } from '@nestjs/swagger';

import { CreateBugDto } from './create-bug.dto';

export class CreateProjectBugDto extends OmitType(CreateBugDto, ['project']) {}
