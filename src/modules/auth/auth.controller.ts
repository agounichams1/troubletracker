import { Body, Controller, Post } from '@nestjs/common';

import { AuthService } from './auth.service';
import { LoginDto, LoginResponse, RegisterDto } from './dto';

import { ApiBearerAuth, ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { User } from "../users/entities";

@ApiBearerAuth()
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Register User' })
  @ApiResponse({ status: 201, description: 'Created' })
  @Post('register')
  async register(@Body() registerDto: RegisterDto): Promise<User> {
    return await this.authService.register(registerDto);
  }

  @ApiOperation({ summary: 'Login User' })
  @ApiResponse({ status: 200, description: 'OK' })
  @Post('login')
  async login(@Body() credentials: LoginDto): Promise<LoginResponse> {
    return await this.authService.login(credentials);
  }
}
