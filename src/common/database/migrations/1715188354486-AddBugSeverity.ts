import { MigrationInterface, QueryRunner } from "typeorm";

export class AddBugSeverity1715188354486 implements MigrationInterface {
    name = 'AddBugSeverity1715188354486'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."bug_severity_enum" AS ENUM('Critical', 'Major', 'Minor')`);
        await queryRunner.query(`ALTER TABLE "bug" ADD "severity" "public"."bug_severity_enum" NOT NULL DEFAULT 'Minor'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "bug" DROP COLUMN "severity"`);
        await queryRunner.query(`DROP TYPE "public"."bug_severity_enum"`);
    }

}
