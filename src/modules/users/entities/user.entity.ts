import { Column, DeleteDateColumn, Entity, Index } from 'typeorm';

import { SoftDeletable } from '../../../common/types';
import { BaseEntity } from 'src/common/base/entities/base.entity';

@Entity('users')
export class User extends BaseEntity implements SoftDeletable {
  @Column({ name: 'first_name' })
  firstName: string;

  @Column({ name: 'last_name' })
  lastName: string;

  @Column({ name: 'email', unique: true })
  @Index({ unique: true })
  email: string;

  @Column({ name: 'password' })
  password: string;

  @Column({ name: 'is_active', default: true })
  isActive: boolean;

  @DeleteDateColumn()
  deletedAt: Date;
}
