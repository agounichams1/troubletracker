import { Module } from '@nestjs/common';
import databaseConfig from './common/database/config/database.config';
import appConfig from './common/config/app.config';
import { ConfigModule } from '@nestjs/config';
import { HomeModule } from './modules/home/home.module';
import { TypeOrmConfigService } from './common/database/typeorm-config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './modules/users/users.module';
import authConfig from './modules/auth/config/auth.config';
import { AuthModule } from './modules/auth/auth.module';
import { BugsModule } from './modules/bugs/bugs.module';
import { ProjectsModule } from './modules/projects/projects.module';
import { StatisticsModule } from './modules/statistics/statistics.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [databaseConfig, authConfig, appConfig],
      envFilePath: ['.env'],
    }),
    TypeOrmModule.forRootAsync({
      useClass: TypeOrmConfigService,
    }),
    AuthModule,
    HomeModule,
    UsersModule,
    BugsModule,
    ProjectsModule,
    StatisticsModule,
  ],
})
export class AppModule {}
