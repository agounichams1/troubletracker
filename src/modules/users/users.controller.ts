import {
  Body,
  Controller,
  Get,
  Param,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';

import { User } from './entities';
import { FilterUsersDto, UpdateUserDto } from './dto';
import { UsersService } from './users.service';

import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards';

@ApiBearerAuth()
@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiOperation({ summary: 'Get User by id' })
  @ApiResponse({ status: 200, description: 'Found user', type: User })
  @ApiResponse({ status: 404, description: 'User not found' })
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getUser(@Param('id') id: number): Promise<User> {
    return await this.usersService.user(id);
  }

  @ApiOperation({ summary: 'Get all Users' })
  @ApiResponse({
    status: 200,
    description: 'List of users',
    type: User,
    isArray: true,
  })
  @ApiResponse({ status: 400, description: 'Invalid filter criteria' }) // Add for potential validation errors
  @UseGuards(JwtAuthGuard)
  @Get()
  async getUsers(
    @Query('filterUsersInput') filterUsersInput: FilterUsersDto,
  ): Promise<User[]> {
    return await this.usersService.users(filterUsersInput);
  }

  @ApiOperation({ summary: 'Update User' })
  @ApiResponse({ status: 200, description: 'Updated user', type: User })
  @ApiResponse({ status: 400, description: 'Invalid update data' }) // Add for potential validation errors
  @ApiResponse({ status: 404, description: 'User not found' })
  @UseGuards(JwtAuthGuard)
  @Put()
  async updateUser(@Body() updateUserInput: UpdateUserDto): Promise<User> {
    return await this.usersService.update(updateUserInput);
  }

  @ApiOperation({ summary: 'Block User' })
  @ApiResponse({ status: 200, description: 'User blocked', type: User })
  @ApiResponse({ status: 404, description: 'User not found' })
  @UseGuards(JwtAuthGuard)
  @Put(':id/block')
  async blockUser(@Param('id') id: number): Promise<User> {
    return await this.usersService.blockUser(id);
  }
}
