import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Bug } from './entities';
import { BugsService } from './bugs.service';

@Module({
  imports: [TypeOrmModule.forFeature([Bug])],
  controllers: [],
  providers: [BugsService],
  exports: [BugsService],
})
export class BugsModule {}
