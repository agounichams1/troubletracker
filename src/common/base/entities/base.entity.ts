import { Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export abstract class BaseEntity {
  @PrimaryGeneratedColumn()
  @Index({ unique: true })
  id: number;
}
